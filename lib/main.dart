import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:async/async.dart';
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Map data;
  List userData;

  Future getUserData() async {
    http.Response response =
        await http.get("https://reqres.in/api/users?page=2");
    data = json.decode(response.body);
    setState(() {
      userData = data["data"];
    });
  }

  /*@override
  void initState() {
    super.initState();
    getUserData();
  }*/

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Fake friends"),
      ),
      body: ListView.builder(
          itemCount: userData == null ? 1 : userData.length,
          itemBuilder: (BuildContext context, int index) {
            if (userData == null) {
              return Center(child: CircularProgressIndicator());
            } else {
              return Card(
                elevation: 20,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundImage:
                            NetworkImage(userData[index]["avatar"]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                        child: Text(
                          "${userData[index]["first_name"]} ${userData[index]["last_name"]}",
                          style: TextStyle(
                              fontSize: 20, fontWeight: FontWeight.w700),
                        ),
                      ),
                    ],
                  ),
                ),
              );
            }
          }),
      bottomNavigationBar: BottomAppBar(
        child: FlatButton(
            color: Colors.deepOrange,
            onPressed: () {
              getUserData();
            },
            child: Text("Get Data")),
      ),
    );
  }
}
